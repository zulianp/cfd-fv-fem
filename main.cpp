#include "utopia_fe.hpp"
#include "utopia_libmesh.hpp"
#include "utopia_MeshTransferOperator.hpp"
#include "utopia_UIScalarSampler.hpp"
#include "utopia_materials.hpp"

#include <cassert>

#include "cNApp.h"
#include "convert_fv_to_fe.hpp"

int main(int argc, char *argv[])
{
	using namespace utopia;
	using PFS = ProductFunctionSpace<LibMeshFunctionSpace>;

	MPI_Init(&argc, &argv);
	PETSC_COMM_WORLD = MPI_COMM_WORLD;

	Utopia::Init(argc, argv);

	{
		libMesh::LibMeshInit init(argc, argv, PETSC_COMM_WORLD);

		cNApp fv;
		fv.init(argc, argv);


		fvfe::ConvertFVToFE c;
		std::shared_ptr<libMesh::MeshBase> mesh = c.apply(init.comm(), fv.getRegion().mesh());

		libMesh::ExodusII_IO io(*mesh);
		io.write("mesh_exp.e");


		 //std::shared_ptr<libMesh::MeshBase> mesh_solid = mesh->clone();

		auto mesh_solid = std::make_shared<libMesh::DistributedMesh>(init.comm());

		int nx = 10, ny = 3, nz = 3;

		 libMesh::MeshTools::Generation::build_cube (
					*mesh_solid,
					nx, ny, nz,
					0, 2.,
					0, 0.2,
					-0.1, 0.1,
					libMesh::TET4
		);


		libMesh::ExodusII_IO io2(*mesh_solid);
		io2.write("mesh_solid.e");

		 //resolid(1, *mesh_solid);

		 auto equation_systems = std::make_shared<libMesh::EquationSystems>(*mesh);
		 equation_systems->add_system<libMesh::LinearImplicitSystem>("fv");

		 auto equation_systems_solid = std::make_shared<libMesh::EquationSystems>(*mesh_solid);
		 equation_systems_solid->add_system<libMesh::LinearImplicitSystem>("fem");

		 PFS V_solid, V;
		 
		 for(int i = 0; i < 3; ++i) {
		 	V_solid *= LibMeshFunctionSpace(equation_systems_solid);
		 	V *= LibMeshFunctionSpace(equation_systems, libMesh::MONOMIAL, libMesh::CONSTANT);
		 }	

		auto u = trial(V_solid);
		auto v = test(V_solid);

		init_constraints(
			constraints(
				boundary_conditions(u[0] == coeff(0.), { 1 }), // dirichlet 0 on boundary 1
				boundary_conditions(u[1] == coeff(0.), { 1 }), // dirichlet 0 on boundary 1
				boundary_conditions(u[2] == coeff(0.), { 1 })  // dirichlet 0 on boundary 1
			)
		);


		 V[0].initialize();
		 V_solid[0].initialize();

		UVector u_fv = local_zeros(V[0].dof_map().n_local_dofs());
		std::cout << V_solid[0].dof_map().n_local_dofs() << " " << V[0].dof_map().n_local_dofs() << std::endl;


		TransferOptions opts;
		opts.from_var_num = 0;
		opts.to_var_num = 0; 
		opts.tags = {};
		opts.n_var = 3;

		MeshTransferOperator op(
			mesh_solid,
			make_ref(V_solid[0].dof_map()),
			mesh,
			make_ref(V[0].dof_map()),
			opts
		);

		op.initialize(utopia::L2_PROJECTION);
		
		auto f = ctx_fun(lambda_fun([](const std::vector<double> &p) -> double {
			return p[0]*p[0] + p[1]*p[1] + p[2]*p[2];
		}));

		auto l_form = inner(f, v[0]) * dX + inner(coeff(1.), v[1]) * dX + inner(coeff(2.), v[2]) * dX;
		auto b_form = inner(u, v) * dX;


		USparseMatrix mass;
		UVector fh;
		utopia::assemble(b_form == l_form, mass, fh);
		UVector lumped = sum(mass, 1);
		UVector u_solid = e_mul(1./lumped, fh);
		op.apply(u_solid, u_fv);

		cfdVolVector3Field pippo(fv.getRegion().fluid_ptr(), "pippo");

		auto pippo_interior = pippo.subArrayForInterior();

		{
			Read<UVector> r(u_fv);
			auto rr = range(u_fv);

			int idx = 0;
			for(auto i = rr.begin(); i < rr.end(); i += 3, ++idx)
			{
				pippo_interior[idx].x() = u_fv.get(i);
				pippo_interior[idx].y() = u_fv.get(i+1);
				pippo_interior[idx].z() = u_fv.get(i+2);
			} 
		}

		pippo.write(fv.getRegion().timeDirectoryPath(), "ascii");
		
		/*
		LameeParameters params;
		LinearElasticity<PFS, USparseMatrix, UVector> elast(V_solid, params);

		USparseMatrix H;
		UVector g;
		UVector x = local_zeros(V_solid[0].dof_map().n_local_dofs());

		elast.assemble_hessian_and_gradient(x, H, g);
		apply_boundary_conditions(V_solid[0].dof_map(), H, g);


		{
			Write<UVector> w(x);
			auto rr = range(x);

			int idx = 0;
			for(auto i = rr.begin(); i < rr.end(); i += 3, ++idx)
			{
				 x.set(i, pippo_interior[idx].x());
				 x.set(i+1, pippo_interior[idx].y());
				 x.set(i+2, pippo_interior[idx].z());
			} 
		}

		GMRES<USparseMatrix, UVector> gmres("bjacobi");
		gmres.update(make_ref(H));
		gmres.apply(g, x);
		*/
		
		/*
		cfdVolScalarField pippo(fv.getRegion().fluid_ptr(), "pippo");

		auto pippo_interior = pippo.subArrayForInterior();

		{
			Read<UVector> r(u_fv);
			auto rr = range(u_fv);
			for(auto i = rr.begin(); i < rr.end(); ++i)
			{
				auto value = u_fv.get(i);
				pippo_interior[i] = value;
			} 
		}

		pippo.write(fv.getRegion().timeDirectoryPath());
		*/


	}


	return Utopia::Finalize();
}
