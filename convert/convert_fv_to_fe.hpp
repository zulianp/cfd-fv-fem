#ifndef CONVERT_FE_TO_FE_HPP
#define CONVERT_FE_TO_FE_HPP

#include <libmesh/libmesh.h>
#include <libmesh/mesh.h>
#include <libmesh/parallel_mesh.h>
#include <libmesh/elem.h>

#include <cfdMesh.h>
#include <cfdTypes.h>

#include "utopia_make_unique.hpp"

#include <set>
#include <memory>
#include <map>

namespace fvfe {

    class ConvertFVToFE {
    public:

        void add_hex(const cfdMesh& fv, label iElement, libMesh::MeshBase& mesh)
        {
            std::map<label, std::set<label>> edges;
            

            auto& elem2face = fv.elementGlobalFaceIndices();
            auto face2node = fv.faceNodeIndicesSubArray();

            auto owner = fv.ownerIndicesSubArrayForAllFaces();
            auto neigh = fv.neighbourIndicesSubArrayForAllFaces();


            label theNumberOfElementFaces = static_cast<label>(elem2face[iElement].size());

            std::vector<std::set<label>> f2n(theNumberOfElementFaces);
          
            for(label iLocalFace=0; iLocalFace<theNumberOfElementFaces; iLocalFace++)
            {
                label iFace = elem2face[iElement][iLocalFace];
                auto f = face2node[iFace];

                for(label i = 0; i < f.size(); ++i) 
                {
                    f2n[iLocalFace].insert(f[i]);

                    label ip1 = (i + 1) % f.size();
                    edges[f[i]].insert(f[ip1]);
                    edges[f[ip1]].insert(f[i]);
                }
            }


            label face0 = elem2face[iElement][0];
            auto f0 = face2node[face0];

            bool is_owner0 = owner[face0] != iElement;

            int iOpposite = -1;
            for(label iLocalFace = 1; iLocalFace < theNumberOfElementFaces; iLocalFace++)
            {
                label iFace = elem2face[iElement][iLocalFace];
                auto f = face2node[iFace];

                bool connected = false;
                for(auto n : f) {
                   connected = f2n[0].find(n) != f2n[0].end();
                   if(connected) break;
                }

                if(!connected) {
                    iOpposite = iLocalFace;
                }
            }

            assert(iOpposite > 0);

            label faceOpposite = elem2face[iElement][iOpposite];
            auto fOpposite = face2node[faceOpposite];

            bool is_ownerOp = owner[faceOpposite] == iElement;

            label n0 = f0[0];
            label idxOpposite = -1;

            for(int i = 0; i < 4; ++i) {
                auto n = fOpposite[i];
                const auto &e = edges[n];
                if(e.find(n0) != e.end()) {
                    idxOpposite = i;
                    break;
                }
            }

            assert(idxOpposite >= 0);

            auto elem = libMesh::Elem::build(libMesh::HEX8);


            int offset = is_owner0? 1 : -1;
            int j = 0;
            for(int idx = 0; idx < 4; ++idx, j += offset) {
                auto n = f0[(j + 4) % 4];       
                auto node_ptr = mesh.node_ptr(n);
                assert(node_ptr);
                
                elem->set_node(idx) = node_ptr;
            }

            offset = is_ownerOp? 1 : -1;
            j = 0;
            for(int idx = 0; idx < 4; ++idx, j += offset) {   
                auto n = fOpposite[(idxOpposite + j + 4) % 4];
                auto node_ptr =  mesh.node_ptr(n);
                assert(node_ptr);
                elem->set_node(4 + idx) = node_ptr;
            }

            for(int idx = 0; idx < 8; ++idx) {
                assert(elem->node_ptr(idx));

                //std::cout << elem->node_ref(idx) << std::endl;
            }

            mesh.add_elem(elem.release());;
        }

        std::unique_ptr<libMesh::MeshBase> apply(
                            libMesh::Parallel::Communicator &comm,
                            const cfdMesh &fv
                )
        {
//            label nfi = fv.numberOfFacesForInterior();
//            label nfb = fv.numberOfAllPatchFaces();
            
            auto mesh = utopia::make_unique<libMesh::DistributedMesh>(comm);
            
            auto nelems = fv.numberOfElements();
            std::vector<std::vector<label>> elem2face(nelems);
//
            auto owner = fv.ownerIndicesSubArrayForAllFaces();
            auto neigh = fv.neighbourIndicesSubArrayForAllFaces();
            auto nf = owner.size();
            
            for(label i = 0; i < nf; ++i) {
                //cout << owner[i] <<" " << neigh[i] << endl;

                elem2face[owner[i]].push_back(i);

                if(neigh[i] != -1) {
                    elem2face[neigh[i]].push_back(i);
                }
            }
            cout << elem2face[0].size() << endl;
            
            auto face2node = fv.faceNodeIndicesSubArray();
            auto points    = fv.nodeCentroidsSubArray();
            auto n_points  = points.size();

            
            libMesh::Point p;
            for(std::size_t i = 0; i < n_points; ++i) {
                p(0) = points[i].x();
                p(1) = points[i].y();
                p(2) = points[i].z();
                
                mesh->add_point(p, i);
            }
            
            std::unique_ptr<libMesh::Elem> elem;
            for(std::size_t i = 0; i < nelems; ++i) {
                //cout << elem2face[i].size() << endl;
                std::set<label> nodes;
                
                assert(elem2face[i].size() <= 6);

                //std::cout << "e: " << i << "\n";
                for(auto f : elem2face[i]) {
                    assert(face2node[f].size() <= 4);

                  //  std::cout << "f(" << f << ") ";
                    for(auto n : face2node[f]) {
                    //    std::cout << n << " ";
                        nodes.insert(n);
                    }

                    //std::cout << std::endl;
                }
                
                auto n_nodes = nodes.size();
                
                switch(n_nodes) {
                    case 4:
                    {
                        elem = libMesh::Elem::build(libMesh::TET4);
                        break;
                    }
                    case 5:
                    {
                        elem = libMesh::Elem::build(libMesh::PYRAMID5);
                        break;
                    }
                    case 6:
                    {
                        elem = libMesh::Elem::build(libMesh::PRISM6);
                        break;
                    }
                    case 8:
                    {
                        add_hex(fv, i, *mesh);
                        break;
                    }
                    default:
                    {
                        assert(false);
                        return nullptr;
                    }
                }                
            }
            
            mesh->prepare_for_use();
            return mesh;
        }
        
    private:
        
    };
    
}


#endif //CONVERT_FE_TO_FE_HPP
